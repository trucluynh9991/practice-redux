import React from 'react';
import Homepages from './pages/homePages/HomePage';
import NotFoudPage from './pages/notFoudPage/NotFoudPage';
import ProductActionPage from './pages/productActionPage/ProductActionPage';
import ProductListPage from './pages/productListPage/ProductListPage';

const routes = [
    {
        path:  '/',
        exact: true,
        main: () => <Homepages/>
    },
    {
        path: '/product-list',
        exact: false,
        main: () => <ProductListPage/>
    },
    {
        path: '/product/add',
        exact: false,
        main: (history) => <ProductActionPage history={history}/>
    },
    {
        path: '/product/:id/edit',
        exact: false,
        main: ({match}) => <ProductActionPage match={match} />
    },
    {
        path: '',
        exact: false,
        main: () => <NotFoudPage/>
    }
];

export default routes;