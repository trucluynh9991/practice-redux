import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import ProductList from '../../components/productList/ProductList';
import ProductItem from '../../components/productItem/ProductItem';
import callApi from '../../untils/apiCaller';
import {Link} from 'react-router-dom'

const mapStateToProps = state => {
    return {
        products: state.products
    }
}
function ProductListPage(props) {
    // let {products} = props; 
    const [products, setProducts]=  useState([]);

    useEffect(() => {
        callApi('sanpham', 'GET', null).then(res => {
            setProducts(res.data)
        });
    }, [])
    
    const onDelete = (id) =>{
        callApi(`sanpham/${id}`, 'DELETE', null).then(res => {
            if(res.status === 200){
                let index = findIndex(products, id);
                if(index !== -1){
                    products.splice(index, 1);
                    setProducts(products)
                }
                console.log(index)
            }
          
        });
    }

    const findIndex = (products, id) =>{
        let result = -1;
        products.forEach((product, index) => {
            if(product.id === id){
                result = index;
            }
        });
        return result;
    }

    const showProduct = (products) =>{
        let result = null;
        if(products.length > 0){
            result = products.map((product,index) =>{
                return (
                    <ProductItem
                        key={index}
                        product={product}
                        index={index}
                        onDelete={onDelete}
                    />
                )
            })
        }
        return result;
    }  
 
    return (
        <div className="col-xs-12">
            <Link to="/product/add" className="btn btn-info">Thêm Sản Phẩm</Link>
            <ProductList>
                {showProduct(products)}
            </ProductList>
        </div> 
    )
}

export default connect(mapStateToProps,null) (ProductListPage);
