import React from 'react'

function NotFoudPage() {
    return (
        <div>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Khong tim thay trang</strong> 
            </div>  
            <script>
              $(".alert").alert();
            </script>
        </div>
    )
}

export default NotFoudPage
