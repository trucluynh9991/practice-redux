import React, {useState, useEffect} from 'react';
import callApi from '../../untils/apiCaller';
import { useHistory, Link } from "react-router-dom";

function ProductActionPage(props) {
    let history = useHistory();
    const [name, setName] = useState({
        id:'', Name: '', Price: '', Status: '' });
    const onChangeName = (e) => {
       setName(
           {...name, Name: e.target.value}
        )
    }
    const onChangePrice = (e) => {
        setName(
            {...name, Price: e.target.value}
         )
     }
     const onChangeStatus = (e) => {
       
        setName(
            {...name, Status: e.target.value ? "false" : "true"}
         )  
     }
     useEffect(() => {
         if(props){
             let id = props.match.params.id;
             callApi(`sanpham/${id}`,'GET',null).then(res => {
                setName({
                    id: res.data.id,
                    Name: res.data.name,
                    Price: res.data.price,
                    Status: res.data.status
                })
             })
         }
     }, [])
    const onSave = (e) =>{
        e.preventDefault();
        let id = props.match.params.id;
        if(id){ //update
            //http://localhost:3000/sanpham:id => HTTP Method: PUT
            callApi(`sanpham/${id}`, 'PUT', {
                name: name.Name,
                price: name.Price,
                status: name.Status
            }).then(res =>{
              history.goBack();
            })
        }
        else{
            callApi('sanpham', 'POST', {
                name: name.Name,
                price: name.Price,
                status: name.Status
            }).then(res =>{
              history.goBack();
            })
        }  
    }

    return (
        <div>
            <form onSubmit={onSave}>
                <div className="form-group">
                    <label>Ten San Pham</label>
                    <input type="text" className="form-control" name="txtName"
                        value={name.Name} onChange = {onChangeName}
                    />            
                </div>
                <div className="form-group">
                    <label>Gia</label>
                    <input type="text" className="form-control" name="txtPrice"
                        value={name.Price} onChange = {onChangePrice}
                    />            
                </div>
                <div className="form-group">
                    <label>Trang Thai</label>     
                </div>
                <div className="checkbox">
                  <label>
                    <input className="ml-10" type="checkbox" name="Status"
                        value={name.Status}
                        checked={name.Status}
                        onChange = {onChangeStatus} 
                    />Con Hang
                  </label>
                </div>
                <Link to="/product-list" className="btn btn-danger">Tro lai</Link>
                <button type="submit" className="btn btn-primary mr-10">Luu lai</button>
            </form>
        
        </div>
    )
}

export default ProductActionPage
