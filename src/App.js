import React from 'react';
import './App.css';
import Menu from './components/menu/Menu';
import routes from './routes';
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';

function App() {
  const showContentMenus = (routes) =>{
    var result = null;
    if(routes.length >0){
      result = routes.map((routes, index) =>{
        return(<Route
          key={index}
          path = {routes.path}
          exact = {routes.exact}
          component = {routes.main}
        />)
      })
    } 
    return  <Switch>{result}</Switch>
  }
  return (
    <Router>
      <div>
        <Menu/>
        <div className="container">
          <div className="row">
           
            {showContentMenus(routes)}
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
