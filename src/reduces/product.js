
const initialState = [
    {
        id: 1,
        name: "Oppo F9",
        price: 200,
        status: true
    },
    {
        id: 2,
        name: "Iphone 7 Plus",
        price: 300,
        status: false
    },
    {
        id: 3,
        name: "Iphone 11 Pro",
        price: 500,
        status: false
    }
];

const products = (state = initialState, action) =>{
    switch(action.type){
        default:
            return [...state];
    }
}
export default products