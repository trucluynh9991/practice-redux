import React from 'react';
import {Link} from 'react-router-dom';

function ProductItem(props) {
    let {product, index} = props;
    let statusName = product.status ? 'Con hang' : 'Het hang';
    let statusClass = product.status ? 'warning' : 'default';

    const onDelete = (id) =>{
        if(confirm('bạn có chắc chắn muốn xóa không?')){ //eslint-disable-line
            props.onDelete(id)
        }
    }
    return (
        <tr>
            <td>{index + 1}</td>
            <td>{product.id}</td>
            <td>{product.name}</td>
            <td>{product.price}</td>
            <td>  
                <span className={`label label-${statusClass}`}>{statusName}</span>
            </td>
            <td>
                <Link to={`product/${product.id}/edit`} className="btn btn-success mr-10">Sửa</Link>
                <button type="button" className="btn btn-danger mr-10" 
                onClick={() => onDelete(product.id)}>Xóa</button>
            </td>
        </tr>
    )
}

export default ProductItem
